package com.json.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.json.model.Student;

@Controller
public class DetailsController {

	@RequestMapping(value="/student", method=RequestMethod.GET)
    public ModelAndView getDetail()
    {
    	System.out.println("In loadDetail()");
		Student stu=new Student();
		stu.setName("Sonu");
		stu.setCity("Bangalore");
		stu.setDegree("btech");
		stu.setRoll(1234);
		ModelAndView mav=new ModelAndView("home.jsp","obj",stu);
		
		
		return mav;
		
    }
	@RequestMapping("studentlist")  
	 public @ResponseBody  
	 List<Student> getStudentList() {  
	  List<Student> studentList = new ArrayList<Student>();  
	  studentList.add(new Student(23, "Meghna", "Naidu", "meghna@gmail.com"  
	    ));  
	  studentList.add(new Student(3, "Robert", "Parera", "robert@gmail.com"  
	    ));  
	  studentList.add(new Student(93, "Andrew", "Strauss",  
	    "andrew@gmail.com"));  
	  studentList.add(new Student(239, "Eddy", "Knight", "knight@gmail.com" 
	    ));  
	  
	  return studentList;  
	 }  
	
}
