package com.json.model;

public class Student {

	private int roll;
	private String name;
	private String degree;
	private String city;
	
	
	public Student(){
		
	}
	
	public Student(int roll, String name, String degree, String city) {
		super();
		this.roll = roll;
		this.name = name;
		this.degree = degree;
		this.city = city;
	}
	public int getRoll() {
		return roll;
	}
	public void setRoll(int roll) {
		this.roll = roll;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDegree() {
		return degree;
	}
	public void setDegree(String degree) {
		this.degree = degree;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	
}
