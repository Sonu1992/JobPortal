<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {font-family: Arial, Helvetica, sans-serif;}
form {border: 3px solid #f1f1f1;}

input[type=text], input[type=password] {
    width: 30%;
    padding: 20px 30px;
    margin: 10px 20px;
    font-size: 25px;
    border: 2px solid #ccc;
    box-sizing: border-box;
}

button {
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    font-size: 16px;
    cursor: pointer;
    width: 10%;
}



.cancelbtn {
    width: auto;
    padding: 10px 18px;
    background-color: #f44336;
}

.imgcontainer {
    text-align: top;
    margin: 24px 0 12px 0;
}

img.avatar {
    width: 40%;
    border-radius: 50%;
}

.container {
    padding: 16px;
}

span.psw {
    float: right;
    padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */

</style>
</head>
<body>

<center><h1 style="color:red;">Admin-Login-Form</h1></center>
<form align="center" action="login" method="post">
  

  <div class="container">
    
    <input type="text" placeholder="Enter Username" name="name" class ="text" required><br>

    
    <input type="password" placeholder="Enter Password" name="password" required><br>
        
    <button type="submit" class="button">Login</button>
    <label>
      <input type="checkbox" checked="checked" name="remember"> Remember me
    </label>
  </div>

  <div class="container" style="background-color:#f1f1f1">
    <tr><td class="cancelbtn">${msg}</td></tr>
    
  </div>
</form>

</body>
</html>
