<html>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script src="./chart.js"></script>
<style>
.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
</style>
<body>
<div id="container"></div>

<center>


<h1>Total Application Recieved:${total}</h1>
<h2>Last Updated Time:${update}</h2>
<h2>Total Read File:${read}</h2> 
<table border="0" cellpadding=10px; bgcolor="#C16D5B  ">
<tr><td>C Language</td><td>DataStructure</td><td>Java</td><td>Android</td><td>Spring:</td><td>Hibernate:</td><td>C++:</td><td>J2EE</td><td>Others</td></tr>
<tr><td>${c}</td><td>${ds}</td><td>${java}</td><td>${andr}</td><td>${spring}</td><td>${hbr}</td><td>${cplus}</td><td>${jee}</td><td>${others}</td></tr>
</table>

</center>

<script>
alert("in Graph");
var c1 = ${c};
var ds1=${ds};
var java1=${java};
var jee1=${jee};
var spring=${spring}
var hibr=${hbr};
var and=${andr};
var cpl=${cplus};



var var1 = new Highcharts.chart({
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',
	renderTo:'container'
    },
    title: {
        text: 'Application Recieved By Technology For Job Code:234012'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Technology',
        colorByPoint: true,
        data: [{
            name: 'Java',
            y: java1
        }, {
        	name: 'J2EE',
            y: jee1
        }, {
            name: 'C',
            y: c1,
            sliced: true,
            selected: true
        }, {
            name: 'Android',
            y: and
        }, {
            name: 'Hibernate',
            y: hibr
        }, {
            name: 'C++',
            y: cpl
        }, {
            name: 'DataStructure',
            y: ds1
        }, {
            name: 'Spring',
            y: spring
        }]
    }]
});
</script>

</body>
</html>
