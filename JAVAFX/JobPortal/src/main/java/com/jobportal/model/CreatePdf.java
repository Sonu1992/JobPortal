package com.jobportal.model;

import java.io.FileOutputStream;
import java.io.IOException;

import org.dom4j.DocumentException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ModelAttribute;


import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
@Component
public class CreatePdf {
    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
            Font.BOLD,BaseColor.GREEN);
    private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.NORMAL, BaseColor.RED);
    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16,
            Font.BOLD);
    private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.BOLD);
    public static final String DEST = "/home/sonukumar/Resumes/draw_rectangle.pdf";
 
   /* public static void main(String[] args) throws IOException, DocumentException {
        System.out.println("In Main;");
    	
        new CreatePdf().createPdf(DEST);
    }*/
    @ModelAttribute
    public void createPdf(String dest,AdmitCard details) throws IOException, DocumentException, com.itextpdf.text.DocumentException {
       System.out.println("In Creating Pdf");
    	Document document = new Document();
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(dest));
        document.open();
        Phrase hello = new Phrase("GLOBAL EDGE:");
        hello.setFont(catFont);
        
        PdfContentByte canvas = writer.getDirectContent();
        ColumnText.showTextAligned(canvas, Element.ALIGN_CENTER,
                hello, 100, 820, 0);
        Rectangle rect = new Rectangle(36, 36, 559, 806);
        rect.setBorder(Rectangle.BOX);
        rect.setBorderWidth(1);
        canvas.rectangle(rect);
        
        
        Paragraph preface = new Paragraph("AdmitCard");
        ColumnText.showTextAligned(canvas, Element.ALIGN_CENTER,
                preface, 310, 780, 0);
        preface.setFont(redFont);
        Rectangle img = new Rectangle(550, 760, 480, 650);
        img.setBorder(Rectangle.BOX);
        img.setBorderWidth(2);
        img.setBorderColor(BaseColor.RED);
        Phrase pic = new Phrase("IMAGE:");
             
        ColumnText.showTextAligned(canvas, Element.ALIGN_CENTER,
        		pic,515, 710, 0);
        canvas.rectangle(img); 
        
        createTable(document,details);
          

        
        document.close();
    }   
    
    static void createTable(Document document,AdmitCard details) throws DocumentException, com.itextpdf.text.DocumentException
    {
    	
    	Paragraph pg=new Paragraph(" ");
    	for (int i = 0; i < 2; i++) 
            pg.add(new Paragraph(" "));
    	pg.add("Candidate Details:-");
        pg.setFont(catFont);
    	pg.add(new Paragraph("..................................................................................................."));
    	document.add(pg);
    	
    	PdfPTable table = new PdfPTable(3);

        PdfPCell c1 = new PdfPCell(new Phrase("Name:"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setPaddingTop(8);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Date of Birth"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setPaddingTop(8);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Email:"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setPaddingTop(8);
        table.addCell(c1);
        table.setHeaderRows(1);

        table.addCell(details.getName());
        table.addCell(details.getDob());
        table.addCell(details.getEmail());
        
        table.setWidthPercentage(80);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        document.add(table);
    	
        
        Paragraph pg3=new Paragraph(" ");
        for (int i = 0; i < 2; i++) 
            pg3.add(new Paragraph(" "));
    	
    	pg3.add("Interview Details:-");
        pg3.setFont(catFont);
    	pg3.add(new Paragraph("..................................................................................................."));
    	document.add(pg3);
        
        PdfPTable table3 = new PdfPTable(3);

        
        
        PdfPCell c3 = new PdfPCell(new Phrase("Date:"));
        c3.setHorizontalAlignment(Element.ALIGN_CENTER);
        c3.setPaddingLeft(10);
        table3.addCell(c3);

        c3 = new PdfPCell(new Phrase("Time:-"));
        c3.setHorizontalAlignment(Element.ALIGN_CENTER);
        c3.setPaddingTop(8);
        table3.addCell(c3);

        c3 = new PdfPCell(new Phrase("Venue:-"));
        c3.setHorizontalAlignment(Element.ALIGN_CENTER);
        c3.setPaddingTop(8);
        table3.addCell(c3);

        table3.setHeaderRows(1);

        table3.addCell(details.getDate());
        table3.addCell(details.getTime());
        table3.addCell(details.getPlace());
        
        table3.setWidthPercentage(90);
        table3.setHorizontalAlignment(Element.ALIGN_CENTER);
        document.add(table3);
        
        
        
        
        
        
        
        
        Paragraph pg2=new Paragraph(" ");
    	for (int i = 0; i < 2; i++) 
            pg2.add(new Paragraph(" "));
    	pg2.add("Academic Details:-");
        pg2.setFont(catFont);
    	pg2.add(new Paragraph("..................................................................................................."));
    	document.add(pg2);
    	
    	PdfPTable table2 = new PdfPTable(3);

        PdfPCell c2 = new PdfPCell(new Phrase("10th Year"));
        c2.setHorizontalAlignment(Element.ALIGN_CENTER);
        c2.setPaddingTop(8);
        table2.addCell(c2);

        c2 = new PdfPCell(new Phrase("12th Year"));
        c2.setHorizontalAlignment(Element.ALIGN_CENTER);
        c2.setPaddingTop(8);
        table2.addCell(c2);

        c2 = new PdfPCell(new Phrase("B.Tech/BE Year"));
        c2.setHorizontalAlignment(Element.ALIGN_CENTER);
        c2.setPaddingTop(8);
        table2.addCell(c2);
        table2.setHeaderRows(1);

        table2.addCell(details.getTenth());
        table2.addCell(details.getTwelve());
        table2.addCell(details.getEngg());
        table2.addCell(details.getTenthmarks());
        table2.addCell(details.getTwelve_marks());
        table2.addCell(details.getEngg_marks());

        table2.setWidthPercentage(70);
        table2.setHorizontalAlignment(Element.ALIGN_CENTER);
        document.add(table2);
    }
    
    private static void addEmptyLine(Paragraph paragraph, int number) throws DocumentException {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
            
        }       
    }
}