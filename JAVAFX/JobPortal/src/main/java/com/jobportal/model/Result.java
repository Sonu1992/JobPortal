package com.jobportal.model;

import javax.persistence.Entity;

@Entity
public class Result {

	private int can_id;
	private String name;
	private String email;
	Candidate can;
	
	public Candidate getCan() {
		return can;
	}
	public void setCan(Candidate can) {
		this.can = can;
	}
	public int getCan_id() {
		return can_id;
	}
	public void setCan_id(int can_id) {
		this.can_id = can_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
	
	
	
	
	
	
}
