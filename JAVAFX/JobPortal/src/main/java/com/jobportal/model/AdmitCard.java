package com.jobportal.model;

public class AdmitCard {

	private String name;
	private String dob;
	private String email;
	
	private String date;
	private String time;
	private String place;
	
	private String tenth;
	private String twelve;
	private String engg;
	
	private String tenthmarks="66%";
	private String twelve_marks="68%";
	private String engg_marks="68.2%";
	
	public String getTenthmarks() {
		return tenthmarks;
	}
	public void setTenthmarks(String tenthmarks) {
		this.tenthmarks = tenthmarks;
	}
	public String getTwelve_marks() {
		return twelve_marks;
	}
	public void setTwelve_marks(String twelve_marks) {
		this.twelve_marks = twelve_marks;
	}
	public String getEngg_marks() {
		return engg_marks;
	}
	public void setEngg_marks(String engg_marks) {
		this.engg_marks = engg_marks;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getTenth() {
		return tenth;
	}
	public void setTenth(String tenth) {
		this.tenth = tenth;
	}
	public String getTwelve() {
		return twelve;
	}
	public void setTwelve(String twelve) {
		this.twelve = twelve;
	}
	public String getEngg() {
		return engg;
	}
	public void setEngg(String engg) {
		this.engg = engg;
	}
	
	
	
}
