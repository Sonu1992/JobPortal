package com.jobportal.dao;

import com.jobportal.model.Candidate;
import com.jobportal.model.Interview;

public interface Dao {

	public String register(Candidate can);
	public Interview interview();
	public Candidate getCard(Candidate can);
}
