package com.jobportal.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import com.jobportal.model.Admin;
import com.jobportal.model.Candidate;
import com.jobportal.model.Experience;
import com.jobportal.model.Fresher;
import com.jobportal.model.Interview;
import com.jobportal.model.Result;
@Repository
public class DaoImp implements Dao {

	public String register(Candidate can){
    String msg;
    int id;
    System.out.println("In REgister");
	Configuration cfg =new Configuration();
    cfg.configure("hibernate.cfg.xml");
	SessionFactory	sf=cfg.buildSessionFactory();
	Session	s=sf.openSession();
	Transaction t=s.beginTransaction();
	Query query = s.createQuery("from Candidate where phno ='"+can.getPhno()+"'");
	List list =null; 
	list = query.list();
	Query emailQuery = s.createQuery("from Candidate where email ='"+can.getEmail()+"'");
	List elist =null; 
	elist = emailQuery.list();
	if(list.size()!=0){
		msg="-:Hi..."+can.getName()+"...You Already Applied:-";
		return msg;
	}
	else if(elist.size()!=0){
		System.out.println("In elist()");
		msg="-:Hi..."+can.getName()+"...You Already Applied:-";
		return msg;
	}
	else{
	id = (Integer) s.save(can);
	t.commit();
	s.close();
	sf.close();
	msg="Hi..."+can.getName()+"...You Applied SuccessFully";
	return msg;
    }		
  }

public Interview interview()
	{
		Interview intr =null;
		Configuration cfg =new Configuration();
	    cfg.configure("hibernate.cfg.xml");
		SessionFactory	sf=cfg.buildSessionFactory();
		Session	s=sf.openSession();
		Transaction t=s.beginTransaction();
		Query query = s.createQuery("from Interview");
		List list =null;
		list=query.list();
		if(list.size()!=0){
			intr=(Interview) list.get(0);
			return intr;
			
		}
		else 
			return intr;
		
	}
	public Candidate getCard(Candidate can){
		Configuration cfg =new Configuration();
	    cfg.configure("hibernate.cfg.xml");
		SessionFactory	sf=cfg.buildSessionFactory();
		Session	s=sf.openSession();
		Transaction t=s.beginTransaction();
		Query query = s.createQuery("from Candidate where phno ='"+can.getPhno()+"'");
		List list =null;
		list=query.list();
		if(list.size()!=0){
			can=(Candidate) list.get(0);
			return can;
			
		}
		else 
			return null;
		
	}
	
	public Admin adminlogin(Admin admin){
	    int id;
	    System.out.println("In ADmin Login");
		Configuration cfg =new Configuration();
	    cfg.configure("hibernate.cfg.xml");
		SessionFactory	sf=cfg.buildSessionFactory();
		Session	s=sf.openSession();
		Transaction t=s.beginTransaction();
		Query query = s.createQuery("from Admin where name ='"+admin.getName()+"'"+"and password='"+admin.getPassword()+"'");
		List list =null; 
		list = query.list();
		if(list.size()!=0){
			return (Admin)list.get(0);
		}
		else 
			return null;
      }
	public String sendInterview(Interview intr){
	    String msg;
	    System.out.println("In SendInterview()...DAo");
	    Configuration cfg =new Configuration();
	    cfg.configure("hibernate.cfg.xml");
		SessionFactory	sf=cfg.buildSessionFactory();
		Session	s=sf.openSession();
		Transaction t=s.beginTransaction();
		Query query = s.createQuery("from Interview");
		List list =null; 
		list = query.list();
		if(list.size()!=0){
			Interview detail;
			detail=(Interview) list.get(0);
			detail.setDate(intr.getDate());
			detail.setTime(intr.getTime());
			detail.setPlace(intr.getPlace());
			s.update(detail);
			t.commit();
			s.close();
			sf.close(); 
			
			return "success";
		}
		else
		return null;
		}
	public String storeResult(Result res){
		System.out.println("In REsult Store");
		int id=0;
		String msg;
		res.setName(res.getCan().getName());
		res.setEmail(res.getCan().getEmail());
		System.out.println("College:--"+res.getCan().getCollege());
		Configuration cfg =new Configuration();
	    cfg.configure("hibernate.cfg.xml");
		SessionFactory	sf=cfg.buildSessionFactory();
		Session	s=sf.openSession();
		Transaction t=s.beginTransaction();
		id = (Integer) s.save(res);
		t.commit();
		s.close();
		sf.close();
        if(id!=0){
		msg="success";
		return msg;
	    }		
        else
        	return null;		
	  }
	public int frPost(Fresher fr){
		int status=0;
		System.out.println("In FreshPost()");
		Configuration cfg =new Configuration();
	    cfg.configure("hibernate.cfg.xml");
		SessionFactory	sf=cfg.buildSessionFactory();
		Session	s=sf.openSession();
		Transaction t=s.beginTransaction();
		status=(int) s.save(fr);
		t.commit();
		s.close();
		sf.close();
		if(status!=0){
			return status;
			
		}
		else 
			return 0;
		
	}
	public int exppost(Experience exp){
		int status=0;
		System.out.println("In Saving expPost()");
		Configuration cfg =new Configuration();
	    cfg.configure("hibernate.cfg.xml");
		SessionFactory	sf=cfg.buildSessionFactory();
		Session	s=sf.openSession();
		Transaction t=s.beginTransaction();
		status=(int) s.save(exp);
		t.commit();
		s.close();
		sf.close();
		if(status!=0){
			return status;
			
		}
		else 
			return 0;
		
	}
	
	
 }
