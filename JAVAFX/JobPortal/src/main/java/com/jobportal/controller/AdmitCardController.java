package com.jobportal.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.jobportal.dao.DaoImp;
import com.jobportal.model.AdmitCard;
import com.jobportal.model.Candidate;
import com.jobportal.model.CreatePdf;
import com.jobportal.model.Interview;
@Controller
public class AdmitCardController {
	@Autowired
	DaoImp dao;
	@Autowired
	CreatePdf adCard;
	public AdmitCardController() {
		System.out.println("AdmitCArd Controller Object Created:");
	}
	public static final String path = "/home/sonukumar/Resumes/admit_card.pdf";
	@RequestMapping(value="/admitcard", method=RequestMethod.POST)
	public ModelAndView generateCard(HttpServletRequest req, HttpServletResponse res,HttpSession session) throws Exception, DocumentException  {
        System.out.println("In Admit Controller");
		Candidate can=new Candidate();       
        can.setPhno(req.getParameter("mb"));
        can=dao.getCard(can);
        if(can!=null){
        	Interview interview=new Interview();
        	interview=dao.interview();
        	if(interview!=null){
        	 AdmitCard mpdf=new AdmitCard();

        	 mpdf.setName(can.getName());
        	 mpdf.setDob(can.getDob());
        	 mpdf.setEmail(can.getEmail());
        	 mpdf.setTenth(can.getTenth());
             mpdf.setTwelve(can.getTwelve());
             mpdf.setEngg(can.getGrad());
             
        	 
        	 mpdf.setDate(interview.getDate());
        	 mpdf.setTime(interview.getTime());
        	 mpdf.setPlace(interview.getPlace());
        	
        	 adCard.createPdf(path,mpdf);
        	 
        	 System.out.println("AdmitCard Created");
        	 ModelAndView mav= new ModelAndView("view");
        	 mav.addObject("name",mpdf.getName());
        	 mav.addObject("dob",mpdf.getDob());
        	 mav.addObject("email",mpdf.getEmail());
        	 return mav;
        	 
        	}
        	else{
        		return new ModelAndView();
        	}
           }
        	else{
        		return new ModelAndView();
        	}
        }

	}
 