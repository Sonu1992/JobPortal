package com.jobportal.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.jobportal.dao.DaoImp;
import com.jobportal.model.Experience;
import com.jobportal.model.Fresher;

@Controller
public class JobPostController {

	@Autowired
	DaoImp dao;
	@Autowired
	AdminController admin;
	@RequestMapping(value="/post", method=RequestMethod.POST)
    public ModelAndView postJob(HttpServletRequest req, HttpServletResponse res)
    {
	System.out.println("In post() ADmin:--"+admin.newadmin.getName());
  	  int status=0;
  	  Fresher fr=new Fresher();
  	  Experience exp=new Experience();
  	  ModelAndView mav=new ModelAndView("insert");
  	  String type=req.getParameter("type");
  	  fr.setLdate(req.getParameter("ldate"));
  	  fr.setJcode(req.getParameter("jcode"));
	  fr.setTenth(req.getParameter("tenth"));
	  System.out.println("Tenth-:"+req.getParameter("10th"));
	  fr.setTwelve(req.getParameter("twelve"));
	  fr.setBtech(req.getParameter("btech"));
	  fr.setPassout(req.getParameter("passout"));
	  fr.setDept(req.getParameter("dept"));
	  
  	  
	  if(type.equals("Fresher")){
  		  System.out.println("In Fresher:-"+type);
  		  fr.setAdminid(admin.newadmin.getId());
  		  status=dao.frPost(fr); 		  
  		  if(status!=0){
  			  mav.addObject("msg"," You Posted Fresher job SuccessFully");
  		      return mav;
  		  }
  		  else{
  			 mav.addObject("msg"," Fresher jobPosting Failed"); 
  		     return mav;
  		  }
  	  }
  	  else{
  		  status=0;
  		  System.out.println("In Experience:-"+type);
  		  exp.setExp(req.getParameter("exp"));
  		  exp.setJcode(req.getParameter("jcode"));
  		  exp.setSkill(req.getParameter("skill"));
  		  exp.setLdate(req.getParameter("ldate"));
  		  exp.setAdminid(admin.newadmin.getId());
  		  System.out.println();
  		  status=dao.exppost(exp);
  		  if(status!=0){
  			mav.addObject("msg"," You Posted Experience job SuccessFully");
  		    return mav; 		    
  		  }
  		  else{
  			mav.addObject("msg"," Job Posting For Experienced Failed");
  		    return mav;
  		  }
  	  }
    }
	
}
