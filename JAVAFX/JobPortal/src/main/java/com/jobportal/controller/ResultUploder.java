package com.jobportal.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.jobportal.dao.DaoImp;
import com.jobportal.model.Candidate;
import com.jobportal.model.Result;

@Controller
public class ResultUploder {

	@Autowired
	DaoImp dao;
	String mb_number;
	Candidate newcan=null;
	@RequestMapping(value="/detail", method=RequestMethod.POST)
	public ModelAndView canDetail(HttpServletRequest req, HttpServletResponse res,HttpSession session) throws Exception, DocumentException  {
        System.out.println("In canDetail Controller()");
		Candidate can=new Candidate();       
        can.setPhno(req.getParameter("mb"));
        newcan=dao.getCard(can);
        mb_number=newcan.getPhno();
        if(can!=null){
        	ModelAndView mav=new ModelAndView("postresult");
        	mav.addObject("name",newcan.getName());
        	mav.addObject("email",newcan.getEmail());
        	mav.addObject("dob",newcan.getDob());
        	return mav;
        }
        else{
        	ModelAndView mav=new ModelAndView("postresult");
        	mav.addObject("msg","Details Not Found");
        	return mav;
        }
	}
	
	@RequestMapping(value="/result", method=RequestMethod.POST)
	public ModelAndView canResult(HttpServletRequest req, HttpServletResponse res,HttpSession session) throws Exception, DocumentException  {
        System.out.println("In canResult Controller()");
		String status=null;
        Result result=new Result();
        System.out.println("Qulified Mobil No=:"+req.getParameter("mb"));               
		result.setCan(newcan);
        status=dao.storeResult(result);
        if(status!=null){
        	ModelAndView mav=new ModelAndView("postresult");
           	mav.addObject("msg","REsulted Uploded SuccessFully:");
        	return mav;
        }
        else{
        	ModelAndView mav=new ModelAndView("postresult");
        	mav.addObject("msg","Result Upload Failed:");
        	return mav;
        }
	}
	
	
	
	
        	
}
