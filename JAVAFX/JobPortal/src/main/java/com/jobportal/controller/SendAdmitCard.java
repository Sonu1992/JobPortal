package com.jobportal.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.jobportal.dao.DaoImp;
import com.jobportal.model.Interview;

@Controller
public class SendAdmitCard {

	@Autowired 
    DaoImp dao;
    @RequestMapping(value="/place", method=RequestMethod.POST)
    public ModelAndView sendAdmitCard(HttpServletRequest req, HttpServletResponse res)
    {
    	System.out.println("In sendAdmitCard();");
  	  String status=null;
  	  Interview intr=new Interview();
  	  intr.setDate(req.getParameter("date"));
  	  intr.setTime(req.getParameter("time"));
  	  intr.setPlace(req.getParameter("place"));
  	  
  	  status=dao.sendInterview(intr);
  	  if(status!=null){
  	  ModelAndView mav=new ModelAndView("insert");
  	  mav.addObject("msg","Date,Time,Place Updates SuccessFully");
  	  return mav;
  	  }
  	  else{
  		ModelAndView mav=new ModelAndView("insert");
    	  mav.addObject("msg","Date,Time,Place Updattion Failed");
    	  return mav;
  	  }
  	  
  		
    }
}
