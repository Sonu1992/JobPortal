package com.jobportal.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.jobportal.dao.DaoImp;
import com.jobportal.model.Admin;

@Controller
public class AdminController {

      @Autowired 
      DaoImp dao;
      Admin newadmin=null;
      @RequestMapping(value="/login", method=RequestMethod.POST)
      public ModelAndView loginAdmin    (HttpServletRequest req, HttpServletResponse res)
      {
    	  System.out.println("In /login()");
    	  
    	  Admin admin=new Admin();
    	  admin.setName(req.getParameter("name"));
    	  admin.setPassword(req.getParameter("password"));
    	  newadmin=dao.adminlogin(admin);
    	  if(newadmin!=null){
    		  return new ModelAndView("admin");
    	  }
    	  else {
    	  ModelAndView mav= new ModelAndView("login");
    	  mav.addObject("msg", "UserNAme/PassWord is Wrong");
    	  return mav;
    	  }
      }
      @RequestMapping(value="/admin", method=RequestMethod.POST)
      public ModelAndView getAdmin(HttpServletRequest req, HttpServletResponse res)
      {
    	  System.out.println("In /admin()");
    		  ModelAndView mav= new ModelAndView("top");
    		  mav.addObject("msg","Welcome..."+newadmin.getName());
    		  return mav;
    	 
      }
      
}
