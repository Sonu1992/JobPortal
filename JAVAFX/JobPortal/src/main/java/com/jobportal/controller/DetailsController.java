package com.jobportal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.jobportal.model.Details;
import com.jobportal.model.ReadDetails;

@Controller
public class DetailsController {
    
	@Autowired
	ReadDetails rd;
	@RequestMapping(value="/details", method=RequestMethod.POST)
    public ModelAndView details()
    {
		System.out.println("In details()");
		Details detail=new Details();
		detail=rd.readDetails(detail);
		ModelAndView mav=new ModelAndView("graph");
		mav.addObject("total",detail.getTotalApplied());
		mav.addObject("update", detail.getLastUpdated());
		mav.addObject("read", detail.getReadFile());
		mav.addObject("c", detail.getC());
		mav.addObject("ds", detail.getDs());
		mav.addObject("java", detail.getJava());
		mav.addObject("jee", detail.getJ2ee());
		mav.addObject("cplus", detail.getCplus());
		mav.addObject("spring", detail.getSpring());
		mav.addObject("hbr", detail.getHibernate());
		mav.addObject("andr", detail.getAndroid());
		mav.addObject("others", detail.getOthers());
		
		return mav;
		
    }
}
