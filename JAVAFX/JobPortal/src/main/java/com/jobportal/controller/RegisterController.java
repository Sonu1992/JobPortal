package com.jobportal.controller;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.jobportal.dao.DaoImp;
import com.jobportal.model.Candidate;

@Controller
public class RegisterController {

	@Autowired
	DaoImp dao;
	String resumePath="/home/sonukumar/Resumes";
	String imgPath ="/home/sonukumar/Desktop/img";
	public String getResumePath() {
		return resumePath;
	}
	public void setResumePath(String resumePath) {
		this.resumePath = resumePath;
	}
	public String getImgPath() {
		return imgPath;
	}
	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}
	public RegisterController() {
		System.out.println("RegisterController Obj Created");
	}
/*	@RequestMapping(value="/reg", method=RequestMethod.POST)
	public void inFun(@RequestParam("img") CommonsMultipartFile file,HttpServletRequest req){
		System.out.println("In inFun()");
		System.out.println(file.getOriginalFilename());
	}*/
	@RequestMapping(value="/reg", method=RequestMethod.POST)
	public ModelAndView regDetails(@RequestParam("img") CommonsMultipartFile file, @RequestParam("file") CommonsMultipartFile file2,HttpServletRequest req, HttpServletResponse res,HttpSession session)  {
        System.out.println("In Reg();....");
		Candidate can=new Candidate();
        can.setName(req.getParameter("name"));
        can.setCollege(req.getParameter("college"));
        can.setDob(req.getParameter("dob"));
        can.setEmail(req.getParameter("email"));
        can.setPhno(req.getParameter("phno"));
        can.setTenth(req.getParameter("tenth"));
        can.setTwelve(req.getParameter("twelve"));
        can.setGrad(req.getParameter("grad"));
        String filename=file.getOriginalFilename();
        String filename2=file2.getOriginalFilename();
        System.out.println(imgPath+" "+filename);  
        byte []buf=file.getBytes();
        byte []buf2=file2.getBytes();
        try {
			BufferedOutputStream bout =new BufferedOutputStream(new FileOutputStream(imgPath+"/"+filename));
			BufferedOutputStream bout2 =new BufferedOutputStream(new FileOutputStream(resumePath+"/"+filename2));
			bout.write(buf);
			bout2.write(buf2);
			bout.flush();  
	        bout.close();  
	        bout2.flush();  
	        bout2.close();  
	        System.out.println("File Uploaded");
		} catch (Exception e) {
			e.printStackTrace();
		}
        can.setResume(getResumePath()+"/"+filename2);
        can.setImg(getImgPath()+"/"+filename);
        String msg=dao.register(can);
              
		ModelAndView mav=new ModelAndView("insert");
		mav.addObject("message", msg);
		return mav;
        
	
	}
}
