<html>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script src="./chart.js"></script>
<style>
.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
</style>
<body>
<div id="container"></div>

<center>
<table>
<h1>Total Application Recieved:${total}</h1></td>
<h2>Last Updated Time:${update}</h2>
<h2>Total Read File:${read}</h2>
<form action="details" method="post">
<div style="position:absolute;padding-top:55px;right:400px">
<button type="submit"class="button">UPDATE</button>
</table>
</div>
</center>

<script>
var var1 = new Highcharts.chart({
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',
	renderTo:'container'
    },
    title: {
        text: 'Application Recieved By Technology For Job Code:234012'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Technology',
        colorByPoint: true,
        data: [{
            name: 'Java/Spring/Hibernate',
            y: 56.33
        }, {
            name: 'C',
            y: 24.03,
            sliced: true,
            selected: true
        }, {
            name: 'Android',
            y: 10.38
        }, {
            name: 'Ios',
            y: 4.77
        }, {
            name: 'C++',
            y: 0.91
        }, {
            name: 'Other',
            y: 0.2
        }]
    }]
});
</script>
</body>
</html>
