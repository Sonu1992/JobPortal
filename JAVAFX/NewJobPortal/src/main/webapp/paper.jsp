<!DOCTYPE html>
<html lang="en">
<head>
  
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style>

.myModal {
   background-color: red;

}

.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}

</style>
<script>
function myFunction() {
    alert('Your Test Time is Over');
    callback();
}

function callback() {
	
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          
          document.getElementById('demo').innerHTML= this.responseText; 
         // document.getElementById('code').value=this.responseText;
        }
      };
    xhr.open("POST", "answer", true);
    xhr.send();
   
    /* ignore result */
  }
</script>
</head>

<body>
<center>

<div class="container">
  <h2 style="color:red;">-:Instructions.:-</h2> 
  
  <p>Number of  Quetions <b>6</b></p>
  <p>Toal Time <b>2</b> minutes</p>
  <p>All Questions Are Mandatory.</p>
  
  <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" onclick="setTimeout(myFunction, 60*1000);">Start Test</button>
</center>
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h2 style="color:red;" id="demo">${msg}</h2>
          <h3 class="modal-title" style="color:green;">All Questions Are Compulsory.</h3>
        </div>
        <div class="modal-body">
  <form action="answer" method="post">       
<table border="0"  bgcolor="green">
<p class="question">1. What is Date Today?</p>
<ul class="answers">
<input type="radio" name="q1" value="a" id="q1a"><label for="q1a">8th-march</label><br/>
<input type="radio" name="q1" value="b" id="q1b"><label for="q1b">10th-march</label><br/>
<input type="radio" name="q1" value="c" id="q1c"><label for="q1c">12th-march</label><br/>
<input type="radio" name="q1" value="d" id="q1d"><label for="q1d">14th-march</label><br/>
</ul>
<p class="question">2. What is  the Capital Of Karnatka?</p>
<ul class="answers">
<input type="radio" name="q2" value="a" id="q1a"><label for="q1a">Chennai</label><br/>
<input type="radio" name="q2" value="b" id="q1b"><label for="q1b">Hyderabad</label><br/>
<input type="radio" name="q2" value="c" id="q1c"><label for="q1c">Bangalore</label><br/>
<input type="radio" name="q2" value="d" id="q1d"><label for="q1d">Pune</label><br/>
</ul>
<p class="question">3. What is the Capital Of America?</p>
<ul class="answers">
<input type="radio" name="q3" value="a" id="q1a"><label for="q1a">Paris</label><br/>
<input type="radio" name="q3" value="b" id="q1b"><label for="q1b">NewYork</label><br/>
<input type="radio" name="q3" value="c" id="q1c"><label for="q1c">London</label><br/>
<input type="radio" name="q3" value="d" id="q1d"><label for="q1d">Washington</label><br/>
</ul>
<p class="question">4. What is the Capital Of Meghalaya?</p>
<ul class="answers">
<input type="radio" name="q4" value="a" id="q1a"><label for="q1a">Agartala</label><br/>
<input type="radio" name="q4" value="b" id="q1b"><label for="q1b">Guwahati</label><br/>
<input type="radio" name="q4" value="c" id="q1c"><label for="q1c">Shillong</label><br/>
<input type="radio" name="q4" value="d" id="q1d"><label for="q1d">Itanagar</label><br/>
</ul>
<p class="question">5. What is the Capital Of Tamilnadu?</p>
<ul class="answers">
<input type="radio" name="q5" value="a" id="q1a"><label for="q1a">Bangalore</label><br/>
<input type="radio" name="q5" value="b" id="q1b"><label for="q1b">Coimbatore</label><br/>
<input type="radio" name="q5" value="c" id="q1c"><label for="q1c">Hyderabad</label><br/>
<input type="radio" name="q5" value="d" id="q1d"><label for="q1d">Chennai</label><br/>
</ul>
<p class="question">6. What is the Capital Of AndhraPradesh?</p>
<ul class="answers">
<input type="radio" name="q6" value="a" id="q1a"><label for="q1a">Bangalore</label><br/>
<input type="radio" name="q6" value="b" id="q1b"><label for="q1b">Coimbatore</label><br/>
<input type="radio" name="q6" value="c" id="q1c"><label for="q1c">Hyderabad</label><br/>
<input type="radio" name="q6" value="d" id="q1d"><label for="q1d">Amarawati</label><br/>
</ul>


</table>
          
    
        </div>
        <div class="modal-footer">
          <center>
<tr><td style="padding-right:200px">
 <button class="button" type="submit" >SUBMIT</button>
</td></tr>
</center>
        </div>
      </div>
    </div>
  </div>
</div>
</form>      
</body>
</html>
