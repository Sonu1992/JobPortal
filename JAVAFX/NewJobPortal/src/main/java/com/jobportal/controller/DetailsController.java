package com.jobportal.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.jobportal.dao.DaoImp;
import com.jobportal.model.Details;
import com.jobportal.model.Fresher;
import com.jobportal.model.ReadDetails;

@Controller
public class DetailsController {
    
	@Autowired
	ReadDetails rd;
	@Autowired
	DaoImp dao;
	Fresher frs=null;
	public String jcode=null;
	@RequestMapping(value="/details", method=RequestMethod.GET)
    public ModelAndView details(HttpServletRequest req)
    {
		AdminController ac=new AdminController();
		System.out.println("In details()");	
		jcode=req.getParameter("jcode");
		ac.setStr(jcode);
		System.out.println("Dynamic JCode:-"+ac.getStr());
		
		Details detail=new Details();
		detail=rd.readDetails(req.getParameter("jcode"));
		if(detail==null){
			ModelAndView mav=new ModelAndView("graph");
			mav.addObject("jcode", "No File Found");
			return mav;
			
		}
		ModelAndView mav=new ModelAndView("graph");
		mav.addObject("total",detail.getTotalApplied());
		mav.addObject("update", detail.getLastUpdated());
		mav.addObject("read", detail.getReadFile());
		mav.addObject("c", detail.getC());
		mav.addObject("ds", detail.getDs());
		mav.addObject("java", detail.getJava());
		mav.addObject("jee", detail.getJ2ee());
		mav.addObject("cplus", detail.getCplus());
		mav.addObject("spring", detail.getSpring());
		mav.addObject("hbr", detail.getHibernate());
		mav.addObject("andr", detail.getAndroid());
		mav.addObject("others", detail.getOthers());
		mav.addObject("jcode", req.getParameter("jcode"));
		
		
		return mav;
		
    }
	public String getJcode() {
		return jcode;
	}
	public void setJcode(String jcode) {
		this.jcode = jcode;
	}
	@RequestMapping(value="/load", method=RequestMethod.GET)
    public ModelAndView loadJob(HttpServletRequest req)
    {
    	ModelAndView mav =new ModelAndView("jobPortal");
		System.out.println("In loadJob()");
	    frs=new Fresher();
		frs=dao.loadFrs(frs);
		mav.addObject("jcode",frs.getJcode());
		mav.addObject("tenth",frs.getTenth());
		mav.addObject("twelve",frs.getTwelve());
		mav.addObject("btech",frs.getBtech());
		mav.addObject("passout",frs.getPassout());
		mav.addObject("dept",frs.getDept());
		mav.addObject("ldate",frs.getLdate());
		
		System.out.println("jcode:-"+frs.getJcode());
		System.out.println("Year:-"+frs.getPassout());
		System.out.println("Btech:-"+frs.getBtech());
		System.out.println("IP:-"+req.getRemoteAddr());
		System.out.println("HOstname:"+req.getRemoteHost());
		System.out.println("Date and Time:-"+new Date());
	    return mav;
		
    }
	
}
