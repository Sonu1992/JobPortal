package com.jobportal.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.jobportal.dao.DaoImp;
import com.jobportal.model.Answer;
import com.jobportal.model.HallId;

@Controller
public class TestController {

	@Autowired 
    DaoImp dao;
	int h_id=0;
	
	@RequestMapping(value="/hallid", method=RequestMethod.POST)
    public ModelAndView getid(HttpServletRequest req, HttpServletResponse res)
    {
  	  System.out.println("In Side Login()...");
  	  String status=null;
  	  
  	  HallId hid=new HallId();
  	  hid.setHallid(Integer.parseInt(req.getParameter("hid")));
  	  h_id=hid.getHallid();
  	  hid=dao.testlogin(hid);
  	  if(hid!=null){
  		   ModelAndView mav=new ModelAndView("paper");
   		   return mav;
  	  }
  	  else {
  	  ModelAndView mav= new ModelAndView("test");
  	  mav.addObject("msg", "-:InValid HallID:-");
  	  return mav;
  	  }
    }
	
	@RequestMapping(value="/answer", method=RequestMethod.POST)
    public ModelAndView answer(HttpServletRequest req, HttpServletResponse res)
    {
  	  System.out.println("In Side Login()...");
  	  String status=null;
  	  
  	  System.out.println("In /answer()");
  	System.out.println("q1:"+req.getParameter("q1"));
  	System.out.println("Q2:"+req.getParameter("q2"));
  	System.out.println("Q3:"+req.getParameter("q3"));
  	System.out.println("Q4:"+req.getParameter("q4"));
  	System.out.println("Q5:"+req.getParameter("q5"));
  	System.out.println("Q6:"+req.getParameter("q6"));
  	  System.out.println("Out /answer()");
  
  	  Answer ans=new Answer();
  	  if(req.getParameter("q1")!=null)
  	  ans.setQone(req.getParameter("q1"));
  	  else
  		ans.setQone("e");
      if(req.getParameter("q2")!=null)
  	  ans.setQtwo(req.getParameter("q2"));
      else
    		ans.setQtwo("e");
      if(req.getParameter("q3")!=null)
      ans.setQthr(req.getParameter("q3"));
      else
    	  ans.setQthr("e");
      if(req.getParameter("q4")!=null)
    	  ans.setQfour(req.getParameter("q4"));
      else
    	  ans.setQfour("e");
      
      if(req.getParameter("q5")!=null)
  	  ans.setQfive(req.getParameter("q5"));
      else
    	  ans.setQfive("e");
      if(req.getParameter("q5")!=null)
      ans.setQsix(req.getParameter("q6"));
      else
    	  ans.setQsix("e");
      
  	  if(h_id!=0){
  	  ans.setHallid(h_id);
  	  int check=dao.saveAnswer(ans);
  	  if(check!=0){
  		   ModelAndView mav=new ModelAndView("insert");
  		   mav.addObject("message", "-:Your Answer is Sumitted SuccessFully:-");		   
  		   return mav;
  	  }
  	  else {
  	  ModelAndView mav= new ModelAndView("insert");
  	  mav.addObject("message", "-:You Already Submited The Answer:-");
  	  return mav;
  	  }
  	 }
  	  else{
  		ModelAndView mav= new ModelAndView("test");
    	 mav.addObject("message", "-:Unable TO find HallId:-");
    	 return mav;
  	  }
    }  	
	
	@RequestMapping(value="/result", method=RequestMethod.POST)
    public ModelAndView myResult(HttpServletRequest req, HttpServletResponse res)
    {
	  int result=0;
	  Answer ans=null;
	  String status=null;
	  System.out.println("In /result()");
	  ModelAndView mav=new ModelAndView("result");
	  int id=Integer.parseInt(req.getParameter("id"));
	  ans=dao.getResult(id);
	  if(ans!=null){
		  if(ans.getQone().equals("a")){
			  result++;
		  }
          if(ans.getQtwo().equals("c")){
			  result++;
		  }
          if(ans.getQthr().equals("d")){
	         result++;
          }
          if(ans.getQfour().equals("c")){
	         result++;
          }
          if(ans.getQfive().equals("d")){
	         result++;
          }
          if(ans.getQsix().equals("d")){
	        result++;
          }
          mav.addObject("info","For Qualifying Cutoff Marks is:= 5");
		  mav.addObject("message","Total Correct Answers="+result);
	      if(result>4)
		  status=dao.storeResult(id);  
		  
		  return mav;
	  }
	  else{
		  mav.addObject("message","Invalid HallID Please Check Your AdmitCard:-");
		  return mav;
	      }
	  
    }
 }

	
	

