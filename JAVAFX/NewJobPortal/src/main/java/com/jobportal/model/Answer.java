package com.jobportal.model;

public class Answer {
	private int id;
	private String qone;
	private String qtwo;
	private String qthr;
	private String qfour;
	private String qfive;
	private String qsix;
	private int hallid;
	
	
	
	public String getQfour() {
		return qfour;
	}
	public void setQfour(String qfour) {
		this.qfour = qfour;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getQone() {
		return qone;
	}
	public void setQone(String qone) {
		this.qone = qone;
	}
	public String getQtwo() {
		return qtwo;
	}
	public void setQtwo(String qtwo) {
		this.qtwo = qtwo;
	}
	public String getQthr() {
		return qthr;
	}
	public void setQthr(String qthr) {
		this.qthr = qthr;
	}
	public String getQfive() {
		return qfive;
	}
	public void setQfive(String qfive) {
		this.qfive = qfive;
	}
	public String getQsix() {
		return qsix;
	}
	public void setQsix(String qsix) {
		this.qsix = qsix;
	}
	public int getHallid() {
		return hallid;
	}
	public void setHallid(int hallid) {
		this.hallid = hallid;
	}
	
	
	

}
