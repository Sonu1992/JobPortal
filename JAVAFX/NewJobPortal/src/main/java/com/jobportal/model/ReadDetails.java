package com.jobportal.model;

import java.io.File;
import java.util.Date;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jobportal.dao.DaoImp;

@Component
public class ReadDetails {
	@Autowired
	DaoImp dao;
	public ReadDetails(){
		System.out.println("ReadDetails Obj Created:");
	}
	public Details readDetails(String code){
		System.out.println("In readDetails()");
		int flag_c=0,flag_java=0,flag_ds=0,flag_spring=0,flag_hbr=0,flag_jee=0,flag_andr=0,flag_cplus=0;
    	int c=0,java=0,ds=0,jee=0,spring=0,hbr=0,others=0,andr=0,read_file=0,res=0,total=0,cplus=0;
    	//Fresher fr=dao.findCode();
        Details detail=new Details();
        
        if(code.contains(".")){
        	File myfile=new File(code);
        	System.out.println("Reading REsume From File:"+code);
        	if(code.contains(".pdf")){
        		
        		try { 
        	         PDDocument document = PDDocument.load(myfile); 
        	       //  System.out.println("PDDocument:"+document);
        	        //   System.out.println("After Load():"+pdf);
        	             document.getClass();

        	             if (!document.isEncrypted()) {
        	              // System.out.println("A/f Encryption:"+pdf);
        	                 PDFTextStripperByArea stripper = new PDFTextStripperByArea();
        	                 stripper.setSortByPosition(true);

        	                 PDFTextStripper tStripper = new PDFTextStripper();

        	                 String pdfFileInText = tStripper.getText(document);
        	                 //System.out.println("Text:" + st);
        	                 
        	 				// split by whitespace
        	                 String lines[] = pdfFileInText.split("\\r?\\n");
        	                 for (String line : lines) {
        	                     String words[]=line.split(" ");
        	                 	int i =0;
        	                 	
        	                 	for(i=0;i<words.length;i++){
        	                 		String str=words[i];
        	                 		if(str.toUpperCase().contains("JAVA"))
        	                             flag_java=1;
        	                 			//System.out.println("Java//..//..//..//..//");
        	                 		if(str.toUpperCase().equals("C") || str.toUpperCase().equals("C,"))
        	                             flag_c=1;
        	                 			//System.out.println("C//..//..//..//..//");
        	                 		if(str.toUpperCase().contains("SPRING"))
        	                             flag_spring=1;
        	                 			//System.out.println("Spring//..//..//..//..//");
        	                 		if(str.toUpperCase().contains("HIBERNATE"))
        	                           flag_hbr=1;
        	                 			//  System.out.println("Hibernate//..//..//..//..//");
        	                 		if(str.toUpperCase().contains("ANDROID"))
        	                            flag_andr=1;
        	                 			// System.out.println("Android//..//..//..//..//");
        	                 		if(str.toUpperCase().contains("STRUCTURE"))
        	                             flag_ds=1;
        	                 		if(str.toUpperCase().contains("J2EE"))                			
        	                             flag_jee=1;
        	                 		if(str.toUpperCase().contains("C++"))                			
        	                            flag_cplus=1;
        	                 			//System.out.println("Data Structure//..//..//..//..//");
        	                 	//	System.out.println(words[i]);
        	                 	}
        	                 }
        	                 res++;
        	                 if(flag_c==1){
        	                 	c++;
        	                 	flag_c=0;
        	                 }
        	                 if(flag_java==1){
        	                 	java++;
        	                 	flag_java=0;
        	                 }
        	                 if(flag_andr==1){
        	                 	andr++;
        	                 	flag_andr=0;
        	                 }
        	                 if(flag_spring==1){
        	                 	spring++;
        	                 	flag_spring=0;
        	                 }
        	                 if(flag_ds==1){
        	                 	ds++;
        	                 	flag_ds=0;
        	                 }
        	                 if(flag_hbr==1){            
        	                 	hbr++;
        	                 	flag_hbr=0;
        	                 }
        	                 if(flag_jee==1){
        	                 	jee++;
        	                 	flag_jee=0;
        	                 }
        	                 if(flag_cplus==1){
        	                  	cplus++;
        	                  	flag_cplus=0;
        	                 }
        	                 
        	             }

        	           }catch(Exception e){
        	         	 System.out.println(e.getMessage());
        	           }        	         
        	       
        	    	detail.setAndroid(andr);
        	    	detail.setC(c);
        	    	detail.setDs(ds);
        	    	detail.setJava(java);
        	    	detail.setCplus(cplus);
        	    	detail.setHibernate(hbr);
        	    	detail.setSpring(spring);
        	    	detail.setJ2ee(jee);
        	    	detail.setTotalApplied(total);
        	        detail.setReadFile(res);     	
        	    	detail.setLastUpdated(new Date().toString());
        	    	detail.setOthers(others);
        	    	detail.setjCode(code);
        		return detail;
        	}
        	System.out.println("file.listFile():-"+myfile.listFiles());
           
        	return null;
        }
       
    	 File file=new File("/home/sonukumar/Resumes"+"/"+code);
    	  System.out.println("Reading REsume From Folder:"+"/home/sonukumar/Resumes"+"/"+code);
        
    	
        	if(file.listFiles()==null){
    		return null;
    	     }
        
    	for(File pdf:file.listFiles()){
 		   total=file.listFiles().length;
 		   System.out.println("InForLoop TO read all Files");
 	    if (pdf.isFile()&&(pdf.getName().substring(pdf.getName().lastIndexOf('.')+1).equals("pdf"))) {
     	System.out.println("In Side.Pdf"+pdf);
 	    	
         try { 
         PDDocument document = PDDocument.load(pdf); 
       //  System.out.println("PDDocument:"+document);
        //   System.out.println("After Load():"+pdf);
             document.getClass();

             if (!document.isEncrypted()) {
              // System.out.println("A/f Encryption:"+pdf);
                 PDFTextStripperByArea stripper = new PDFTextStripperByArea();
                 stripper.setSortByPosition(true);

                 PDFTextStripper tStripper = new PDFTextStripper();

                 String pdfFileInText = tStripper.getText(document);
                 //System.out.println("Text:" + st);
                 
 				// split by whitespace
                 String lines[] = pdfFileInText.split("\\r?\\n");
                 for (String line : lines) {
                     String words[]=line.split(" ");
                 	int i =0;
                 	
                 	for(i=0;i<words.length;i++){
                 		String str=words[i];
                 		if(str.toUpperCase().contains("JAVA"))
                             flag_java=1;
                 			//System.out.println("Java//..//..//..//..//");
                 		if(str.toUpperCase().equals("C") || str.toUpperCase().equals("C,"))
                             flag_c=1;
                 			//System.out.println("C//..//..//..//..//");
                 		if(str.toUpperCase().contains("SPRING"))
                             flag_spring=1;
                 			//System.out.println("Spring//..//..//..//..//");
                 		if(str.toUpperCase().contains("HIBERNATE"))
                           flag_hbr=1;
                 			//  System.out.println("Hibernate//..//..//..//..//");
                 		if(str.toUpperCase().contains("ANDROID"))
                            flag_andr=1;
                 			// System.out.println("Android//..//..//..//..//");
                 		if(str.toUpperCase().contains("STRUCTURE"))
                             flag_ds=1;
                 		if(str.toUpperCase().contains("J2EE"))                			
                             flag_jee=1;
                 		if(str.toUpperCase().contains("C++"))                			
                            flag_cplus=1;
                 			//System.out.println("Data Structure//..//..//..//..//");
                 	//	System.out.println(words[i]);
                 	}
                 }
                 res++;
                 if(flag_c==1){
                 	c++;
                 	flag_c=0;
                 }
                 if(flag_java==1){
                 	java++;
                 	flag_java=0;
                 }
                 if(flag_andr==1){
                 	andr++;
                 	flag_andr=0;
                 }
                 if(flag_spring==1){
                 	spring++;
                 	flag_spring=0;
                 }
                 if(flag_ds==1){
                 	ds++;
                 	flag_ds=0;
                 }
                 if(flag_hbr==1){            
                 	hbr++;
                 	flag_hbr=0;
                 }
                 if(flag_jee==1){
                 	jee++;
                 	flag_jee=0;
                 }
                 if(flag_cplus==1){
                  	cplus++;
                  	flag_cplus=0;
                 }
                 
             }

           }catch(Exception e){
         	 System.out.println(e.getMessage());
           }
         }
       }
    	detail.setAndroid(andr);
    	detail.setC(c);
    	detail.setDs(ds);
    	detail.setJava(java);
    	detail.setCplus(cplus);
    	detail.setHibernate(hbr);
    	detail.setSpring(spring);
    	detail.setJ2ee(jee);
    	detail.setTotalApplied(total);
        detail.setReadFile(res);     	
    	detail.setLastUpdated(new Date().toString());
    	detail.setOthers(others);
    	detail.setjCode(code);
 	   System.out.println("Resumes Applied By Technology..");
 	   System.out.println("Total Files Uploaded:"+total);
 	   System.out.println("Total Pdf Files Uploded:"+new Date().getTime());
 	   System.out.println("Total Read Filter Resumes:"+res);
 	   System.out.println("................................."+"/n");
 	   System.out.println("C Language:"+c);
 	   System.out.println("Data Structure:"+ds);
 	   System.out.println("Java:"+java);
 	   System.out.println("J2EE:"+jee);
 	   System.out.println("Android:"+andr);
 	   System.out.println("Spring Framework:"+spring);
 	   System.out.println("Hibernate:"+hbr);
 	   
     	
    	
    	return detail;
	}

	public String setSkill(Details detail){
		String skill="";
        System.out.println("In SideSet Skill");
		if(detail.getC()!=0){
			skill="c"+"/";
		}
		if(detail.getAndroid()!=0){
			skill=skill+"android"+"/";
		}
		if(detail.getCplus()!=0){
			skill=skill+"cpp"+"/";
		}
		
		if(detail.getDs()!=0){
			skill=skill+"datastructure"+"/";
		}
		if(detail.getHibernate()!=0){
			skill=skill+"hibernate"+"/";
		}
		if(detail.getJ2ee()!=0){
			skill=skill+"j2ee"+"/";
		}
		if(detail.getJava()!=0){
			skill=skill+"java"+"/";
		}
		if(detail.getSpring()!=0){
			skill=skill+"spring"+"/";
		}
		
		return skill;
	}
	
	
	
	
	
}
