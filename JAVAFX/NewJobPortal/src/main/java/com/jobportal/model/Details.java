package com.jobportal.model;

public class Details {

	private int totalApplied;
	private String lastUpdated;
	private int readFile;
	private int c;
	private int ds;
	private int java;
	private int android;
	private int hibernate;
	private int spring;
	private int cplus;
	private int others;
	private String jCode;
	
	
	public String getjCode() {
		return jCode;
	}
	public void setjCode(String jCode) {
		this.jCode = jCode;
	}
	public int getOthers() {
		return others;
	}
	public void setOthers(int others) {
		this.others = others;
	}
	public int getJ2ee() {
		return j2ee;
	}
	public void setJ2ee(int j2ee) {
		this.j2ee = j2ee;
	}
	private int j2ee;

	
	public int getTotalApplied() {
		return totalApplied;
	}
	public void setTotalApplied(int totalApplied) {
		this.totalApplied = totalApplied;
	}
	public String getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	public int getReadFile() {
		return readFile;
	}
	public void setReadFile(int readFile) {
		this.readFile = readFile;
	}
	public int getC() {
		return c;
	}
	public void setC(int c) {
		this.c = c;
	}
	public int getDs() {
		return ds;
	}
	public void setDs(int ds) {
		this.ds = ds;
	}
	public int getJava() {
		return java;
	}
	public void setJava(int java) {
		this.java = java;
	}
	public int getAndroid() {
		return android;
	}
	public void setAndroid(int android) {
		this.android = android;
	}
	public int getHibernate() {
		return hibernate;
	}
	public void setHibernate(int hibernate) {
		this.hibernate = hibernate;
	}
	public int getSpring() {
		return spring;
	}
	public void setSpring(int spring) {
		this.spring = spring;
	}
	public int getCplus() {
		return cplus;
	}
	public void setCplus(int cplus) {
		this.cplus = cplus;
	}
	
	
	
	
	
	
}
